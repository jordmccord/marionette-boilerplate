'use strict';

var Marionette = require('backbone.marionette'),
    RouteController = require('../controller/controller');

module.exports = Marionette.AppRouter.extend({
    appRoutes: {
        '': "start",
        'receipt': 'showReceipt'
    },
    initialize: function(options){
        this.controller = new RouteController(this.options);
    }
});