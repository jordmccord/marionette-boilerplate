'use strict';

var Marionette = require('backbone.marionette'),
    LayoutTemplate = require('../templates/layout.hbs');

module.exports = Marionette.LayoutView.extend({
    template: LayoutTemplate,
    regions: {
        'bets': '#bets',
        'slip': '#slip'
    }
});