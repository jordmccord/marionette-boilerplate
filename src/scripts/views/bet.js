'use strict';

var Backbone = require('backbone'),
    Marionette = require('backbone.marionette'),
    BetsTemplate = require('../templates/bet.hbs'),
    BetModel = require('../models/bet');

module.exports = Marionette.ItemView.extend({
    className: 'market',
    template: BetsTemplate,
    model: BetModel,
    ui:{
        'addBtn': '#select-bet-button'
    },
    events:{
        "click @ui.addBtn": 'addToSlip'
    },
    addToSlip(){
        // Trigger bet added to slip event
        Backbone.Radio.channel('betting.slip').trigger('slip:add', this.model.toJSON());
    }
});