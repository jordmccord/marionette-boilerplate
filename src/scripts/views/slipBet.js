'use strict';

var $ = require('jquery'),
    Backbone = require('backbone'),
    Marionette = require('backbone.marionette'),
    SlipBetTemplate = require('../templates/slipBet.hbs'),
    BetModel = require('../models/bet');

module.exports = Marionette.ItemView.extend({
    className: 'slip-bet',
    template: SlipBetTemplate,
    model: BetModel,
    ui:{
        'removeBtn': '#remove-bet-button',
        'stakeValue': '#stake-value'
    },
    events:{
        'click @ui.removeBtn': 'removeSelf',
        'keyup @ui.stakeValue': 'setStakeValue',
        'focus @ui.stakeValue': 'clearInput'
    },
    clearInput(){
        // Check if the value has been set already, if not clear the input
        if(this.ui.stakeValue.val() === '0.00') this.ui.stakeValue.val('');
    },
    setStakeValue(thing){
        var val = this.ui.stakeValue.val(),
            stake = parseFloat(val),
            numer = this.model.get('odds').numerator,
            domin = this.model.get('odds').denominator;

        // Check if a value has been set
        if(this.ui.stakeValue.val().length > 0){
            // Check if the input has a full stop and if there are more than 3 characters after the full stop
            if(val.indexOf('.') > -1 && val.substr(val.indexOf('.')).length > 3){
                // If more than 3 characters after full stop remove one
                this.ui.stakeValue.val(val.substr(0, val.length - 1));
            }

            // Set the bet return and stake on the model
            this.model.set({
                return: stake * numer / domin + stake,
                stake: stake
            });

            // Trigger the bet stake value change event
            Backbone.Radio.channel('betting.slip').trigger('slip:value');
        }
    },
    removeSelf(){
        // Trigger the remove from slip event
        Backbone.Radio.channel('betting.slip').trigger('slip:remove', this.model);
    }
});