'use strict';

var _ = require('underscore'),
    Marionette = require('backbone.marionette'),
    ReceiptView = require('../views/reciept'),
    ReceiptsTemplate = require('../templates/receipts.hbs');

module.exports = Marionette.CompositeView.extend({
    className: 'markets',
    template: ReceiptsTemplate,
    childView: ReceiptView,
    childViewContainer: '#receipts-container',
    ui:{
        'totalReturn': '#total-return'
    },
    onShow(){
        var _this = this,
            stakeReturn = 0;

        // Loop over each receipt and calculate return for each then add to get total
        _.each(_this.collection.toJSON(), function(model){
            var stake = parseFloat(model.stake),
                numer = model.odds.numerator,
                domin = model.odds.denominator;

             stakeReturn = (stake * numer / domin + stake) + stakeReturn;
        });
        this.ui.totalReturn.text(stakeReturn.toFixed(2));
    }
});