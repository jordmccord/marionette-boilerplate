'use strict';

var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone'),
    Marionette = require('backbone.marionette'),
    SlipBetView = require('../views/slipBet'),
    SlipTemplate = require('../templates/slip.hbs');

module.exports = Marionette.CompositeView.extend({
    className: 'slip',
    template: SlipTemplate,
    childView: SlipBetView,
    childViewContainer: '#bets-container',
    ui:{
        'errorMessage': '#error-message',
        'clearBtn': '#clear-button',
        'slipReturn': '#slip-return',
        'placeBetBtn': '#place-bet-button'
    },
    events:{
        'click @ui.clearBtn': 'clearSlip',
        'click @ui.placeBetBtn': 'placeBet'
    },
    addToSlip(model){
        // Hide error message if displayed
        this.ui.errorMessage.hide();
        // Add the model to the view's collection
        this.collection.add(model);
    },
    updateReturn(){
        var _this = this,
            slipReturn = 0;
        // Loop over bets and add up total
        _.each(_this.collection.toJSON(), function(model){
            slipReturn = slipReturn + model.return;
        });
        // Show the slip return in the UI to two decimal places
        this.ui.slipReturn.text(slipReturn.toFixed(2));
    },
    clearSlip(){
        // Clear all markets from the slip
        this.collection.reset(null);
        // Hide error message if displayed
        this.ui.errorMessage.hide();
        // Update return amount
        this.updateReturn();
    },
    handleError(text){
        // Display error message and show error text
        this.ui.errorMessage.show().text(text);
    },
    processBets(){
        var _this = this,
            deferred = $.Deferred(),
            receipts = [];

        // Loop over each bet and make a post for each
        _.each(this.collection.models, (model) => {
            model.bet({
                bet_id: model.get('bet_id'),
                odds: model.get('odds'),
                stake: model.get('stake')
            }).then((model, res) => {
                // Push bet to receipts array
                receipts.push(res);
                // If last bet to be processed resolve the promise with the reciepts array
                if(model === _this.collection.last()) deferred.resolve(receipts);
            }, (model, res) => {
                // If error display error message
                _this.handleError('Something went wrong. Please check your bets and try again.');
                deferred.reject(model, res);
            });
        });

        return deferred.promise();
    },
    placeBet(){
        // Process bets then route to receipts page with the reciepts array
        this.processBets().then((receipts) => {
            Backbone.Radio.channel('betting.slip').reply('bet:receipt', receipts);
            window.location.hash = 'receipt';
        });
    },
    initialize(){
        var _this = this;

        //Listen for a market to trigger slip add event
        Backbone.Radio.channel('betting.slip').on('slip:add', (model) => {
            //Check if collection already contains bet
            if(_this.collection.findWhere({bet_id: model.bet_id})){
                // If so show an error message
                _this.handleError('Market already added.');
            }else{
                // If not add model to the collection
                _this.addToSlip(model);
            }
        });
        // Listen for market to be removed from slip
        Backbone.Radio.channel('betting.slip').on('slip:remove', (model) => {
            // Remove model from the collection
            _this.collection.remove(model);
            _this.updateReturn();
        });

        // Listen for value changes on bet inputs then update the total return
        Backbone.Radio.channel('betting.slip').on('slip:value', (value) => {
            _this.updateReturn();
        });
    }
});