'use strict';

var Marionette = require('backbone.marionette'),
    SpinnerTemplate = require('../templates/spinner.hbs');

module.exports = Marionette.ItemView.extend({
    template: SpinnerTemplate
});