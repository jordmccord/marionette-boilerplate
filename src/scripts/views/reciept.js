'use strict';

var Backbone = require('backbone'),
    Marionette = require('backbone.marionette'),
    ReceiptTemplate = require('../templates/receipt.hbs'),
    ReceiptModel = require('../models/receipt');

module.exports = Marionette.ItemView.extend({
    className: 'market',
    template: ReceiptTemplate,
    model: ReceiptModel,
    ui:{
        'betReturn': '#bet-return'
    },
    onRender(){
        var stake = parseFloat(this.model.get('stake')),
            numer = this.model.get('odds').numerator,
            domin = this.model.get('odds').denominator;

        // Calculate receipt return and show in UI
        this.ui.betReturn.text((stake * numer / domin + stake).toFixed(2));
    }
});