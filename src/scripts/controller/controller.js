'use strict';

var _ = require('underscore'),
    Backbone = require('backbone'),
    Marionette = require('backbone.marionette'),
    LayoutView = require('../views/layout'),
    BetsView = require('../views/bets'),
    SpinnerView = require('../views/spinner'),
    SlipView = require('../views/slip'),
    BetsCol = require('../collections/bets'),
    ReceiptsCol = require('../collections/receipts'),
    ReceiptsView = require('../views/receipts');

module.exports = Marionette.Controller.extend({
    _radioChannel: 'betting.slip',
    start(){
        var _this = this,
            layoutView = new LayoutView(),
            betsCollection = new BetsCol(),
            slipCollection = new BetsCol();

        // Show the layout view
        this.options.App.main.show(layoutView);

        // Display Loading spinner
        layoutView.bets.show(new SpinnerView());

        // Load the bets from the server
        betsCollection.load().then(() => {
            // Once bets have been returned successfully show the bets view instead of the spinner and pass the collection to the view
            layoutView.bets.show(new BetsView({
                collection: betsCollection
            }));
        });

        // Show the slip view with a bet collection
        layoutView.slip.show(new SlipView({
            collection: slipCollection
        }));
    },
    showReceipt(){
        // Check if there is any receipt data
        if(Backbone.Radio.channel('betting.slip').request('bet:receipt')){
            var receipts = new ReceiptsCol();

            // Add the receipt data to the collection
            receipts.add(Backbone.Radio.channel('betting.slip').request('bet:receipt'));

            // Show the receipt view with the retrieved receipts
            this.options.App.main.show(new ReceiptsView({
                collection: receipts
            }));

        }else{
            // If no receipt data return to first page
            window.location.hash = '';
        }
    },
    initialize(){
        // Initialise the app radio channel
        Backbone.Radio.channel(this._radioChannel);
    }
});