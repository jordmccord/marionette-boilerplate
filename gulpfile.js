"use strict";

var gulp        = require('gulp');
var browserify  = require('browserify');
var concat      = require('gulp-concat');
var source      = require('vinyl-source-stream');
var uglify      = require('gulp-uglify');
var hbsfy       = require("hbsfy");
var minifyCSS   = require('gulp-minify-css');
var babel       = require('babelify');
var gutil       = require('gulp-util');

var config = {
	paths: {
		html: './src/*.html',
		js: './src/**/*.js',
		images: './src/img/*',
		css: [
      		'src/styles/style.css'
    	],
		dist: './dist',
		mainJs: './src/scripts/app.js'
	}
};


gulp.task('html', function() {
	gulp.src(config.paths.html)
		.pipe(gulp.dest(config.paths.dist));
});

gulp.task('js', function() {
    hbsfy.configure({
        extensions: ['hbs']
    });

	browserify(config.paths.mainJs)
        .transform(hbsfy)
		.transform(babel)
		.bundle()
		.on('error',  gutil.log)
		.pipe(source('bundle.js'))
		.pipe(gulp.dest(config.paths.dist + '/scripts'))
});

gulp.task('css', function() {
	gulp.src(config.paths.css)
		.pipe(concat('style.css'))
		.pipe(gulp.dest(config.paths.dist + '/styles'));
});

gulp.task('images', function () {
    gulp.src(config.paths.images)
        .pipe(gulp.dest(config.paths.dist + '/img'));

    gulp.src('./src/favicon.ico')
        .pipe(gulp.dest(config.paths.dist));
});

gulp.task('minify', function() {
    return gulp.src('./dist/styles/style.css')
        .pipe(minifyCSS())
        .pipe(gulp.dest('./dist/styles'));
});

gulp.task('uglify', function() {
    return gulp.src('./dist/scripts/bundle.js')
        .pipe(uglify().on('error', gutil.log))
        .pipe(gulp.dest('./dist/scripts'));
});

gulp.task('watch', function() {
	gulp.watch(config.paths.html, ['html']);
	gulp.watch(config.paths.js, ['js']);
	gulp.watch(config.paths.css, ['css']);
});

gulp.task('default', ['html', 'js', 'css', 'images', 'watch']);
gulp.task('build', ['uglify', 'minify']);